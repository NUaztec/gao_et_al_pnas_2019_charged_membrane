This repository contains the data associated with the paper:

C. Gao, S. Kewalramani, D. Maria Valencia, H. Li, J. M. McCourt, M. Olvera de la Cruz, and M. J. Bedzyk, Electrostatic shape control of a charged molecular
membrane from ribbon to scroll, Proc. Natl. Acad. Sci. USA 2019, DOI: www.pnas.org/cgi/doi/10.1073/pnas.1913632116
