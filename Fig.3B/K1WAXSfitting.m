clear all

clf

load bkg_sub_C16K1_WAXS_pH5.dat %background_subtracted data truncated to 3.2 �^-1

q = bkg_sub_C16K1_WAXS_pH5(:,1); 

I = bkg_sub_C16K1_WAXS_pH5(:,2);

q2 = 1:0.001:3;

n = length(q2);

%__________________________________________________________________________
%Define variables for angular integration of rods
%__________________________________________________________________________
th = pi/360:pi/360:pi; 

p = length(th);

Dth = pi/360;

%__________________________________________________________________________
%Define fitting parameters
%__________________________________________________________________________

a = 8.52; %Lattice parameter a 

b = 4.95; %Lattice parameter b

gamma = 100.3*pi/180; %Lattice parameter angle

hx = 4; %Headgroup length

hy = 10; %Double headgroup width 
 
hz = 4; %Headgroup thickness

tx = 3.5; %Tail length

ty = 2.5; %Tail width

tz = 13; %Tail thickness

tail = 13; %Bilayer hydrophobic thickness

dh = 0; %ED of head

dt = 3.3; %ED of tail

Deb = 0; %Debye-Waller factor

Scale = 9.5e-12; %Scale factor

orix1 = 0*pi/180;  %Molecular orientation of corner molecule along x axis

oriy1 = 0*pi/180;  %Molecular orientation of corner molecule along y axis

oriz1 = 0*pi/180;  %Molecular orientation of corner molecule headgeoup along z axis

orix2 = orix1; %Molecular orientation of center molecule along x axis

oriy2 = oriy1; %Molecular orientation of center molecule along y axis

oriz2 = 0*pi/180; %Molecular orientation of center molecule headgroup along z axis
%__________________________________________________________________________
% Useful intermediate parameters
%__________________________________________________________________________

a_rec = 2 * pi/(a*sin(gamma));

b_rec = 2 * pi/(b*sin(gamma));

I_fin = zeros(n,1);

A = [0 0];

F0 = zeros(n,360);

Re = zeros(n,360);

Im = zeros(n,360);

Re0 = zeros(n,360);

Im0 = zeros(n,360);

Rec = zeros(n,360);

Imc = zeros(n,360);

Fc = zeros(n,360);

F = zeros(n,360);

S = zeros(n,360);

Is = zeros(n,1);

dq = 0.00001;

h = zeros(11,1);

k = zeros(11,1);

for r = 1:7
    
   for s = 1:7

h(r) = r - 4;
        
k(s) = s - 4;

A = [h(r) k(s)]  % Tracking the progress through diffraction indices

if h(r)*k(s) == -1
    
    L_Dom = 150;
    
elseif h(r)==0 && abs(k(s))==2
    
    L_Dom = 180;
    
else
    L_Dom = 140;
end
    
     if ((h(r) * a_rec)^2 + (k(s) * b_rec)^2 + 2*h(r)*k(s)*a_rec*b_rec*cos(pi-gamma)) > q2(n)^2 || ((h(r) * a_rec)^2 + (k(s) * b_rec)^2 + 2*h(r)*k(s)*a_rec*b_rec*cos(pi-gamma)) < q2(1)^2
                
     Is = zeros(n,1);
                       
     elseif ((h(r) * a_rec)^2 + (k(s) * b_rec)^2 + 2*h(r)*k(s)*a_rec*b_rec*cos(pi-gamma)) <= q2(n)^2 && ((h(r) * a_rec)^2 + (k(s) * b_rec)^2 + 2*h(r)*k(s)*a_rec*b_rec*cos(pi-gamma)) >= q2(1)^2
            
     Qy = (h(r)*a_rec+k(s)*b_rec*cos(pi-gamma))*cos(oriz1)-k(s)*b_rec*sin(pi-gamma)*sin(oriz1)+dq;

     Qx = k(s)*b_rec*sin(pi-gamma)*cos(oriz1)+(h(r)*a_rec+k(s)*b_rec*cos(pi-gamma))*sin(oriz1)+dq;

     Qyc = (h(r)*a_rec+k(s)*b_rec*cos(pi-gamma))*cos(oriz2)-k(s)*b_rec*sin(pi-gamma)*sin(oriz2)+dq;

     Qxc = k(s)*b_rec*sin(pi-gamma)*cos(oriz2)+(h(r)*a_rec+k(s)*b_rec*cos(pi-gamma))*sin(oriz2)+dq;
           
     sig_Dom = 2 * pi/(L_Dom*2.35);
     
%__________________________________________________________________________
%Defining the reciprocal lattice range and intensity calculation
%__________________________________________________________________________
     Q = sqrt(Qx^2+Qy^2);

     for l = 1:n
         
        if q2(l) < (Q+0.6) && q2(l) > (Q-0.2)
         
        for m = 1:p
    
        Qz = q2(l)*cos(th(m));
        
        Qx1 = cos(oriy1)*Qx+sin(oriy1)*Qz;
        
        Qy1 = sin(orix1)*sin(oriy1)*Qx+cos(orix1)*Qy-sin(orix1)*cos(oriy1)*Qz;
        
        Qz1 = -cos(orix1)*sin(oriy1)*Qx+sin(orix1)*Qy+cos(orix1)*cos(oriy1)*Qz;
        
        Qx2 = cos(oriy2)*Qxc+sin(oriy2)*Qz;
        
        Qy2 = sin(orix2)*sin(oriy2)*Qxc+cos(orix2)*Qyc-sin(orix2)*cos(oriy2)*Qz;
        
        Qz2 = -cos(orix2)*sin(oriy2)*Qxc+sin(orix2)*Qyc+cos(orix2)*cos(oriy2)*Qz;  %Coordination transform for corner and center molecule      
        
        E = -(q2(l)*sin(th(m))-sqrt(Qx^2+Qy^2))^2/(2*sig_Dom^2);
         
        S(l,m) = exp(E)*sqrt(sin(th(m)))/(sig_Dom^2*(q2(l)*sqrt(Qx^2+Qy^2))^0.5);   %Stucture factor
        
        %F0(l,m) = -dh*(2*sin(Qx1*hx/2)*(cos(Qy1*hy/2)+1i*sin(Qy1*hy/2)-cos(Qy1*ty/2)+isin(Qy1*hy/2))*(cos(Qz1*hz)-1+isin(Qz1*hz)))/(Qx1*Qy1*Qz1)+...
            %dt*(2*sin(Qx1*tx/2)*2*sin(Qy1*ty/2)*(1-cos(Qz1*tz)+isin(Qz1*tz))/(iQx1*Qy1*Qz1));   %Form factor for corner molecule
        Re0(l,m) = (1/(Qx1*Qy1*Qz1))*dh*2*sin(Qx1*hx/2)*((cos(Qy1*hy/2)-cos(Qy1*ty/2))*(1-cos(Qz1*hz))+(sin(Qy1*hy/2)+sin(Qy1*ty/2))*sin(Qz1*hz))+...
            (1/(Qx1*Qy1*Qz1))*dt*4*sin(Qx1*tx/2)*sin(Qy1*ty/2)*sin(Qz1*tz);
        
        Im0(l,m) = (1/(Qx1*Qy1*Qz1))*dh*2*sin(Qx1*hx/2)*((sin(Qy1*hy/2)+sin(Qy1*ty/2))*(1-cos(Qz1*hz))-sin(Qz1*hz)*(cos(Qy1*hy/2)-cos(Qy1*ty/2)))+...
            (1/(Qx1*Qy1*Qz1))*dt*4*sin(Qx1*tx/2)*sin(Qy1*ty/2)*(cos(Qz1*tz)-1);
        
            
        %Fc(l,m) = -dh*(2*sin(Qx2*hx/2)*(cos(Qy2*ty/2)+isin(Qy2*ty/2)-cos(Qy2*hy/2)+isin(Qy2*hy/2))*(1-cos(Qz2*hz)+isin(Qz2*hz)))/(Qx2*Qy2*Qz2)+...
            %dt*(2*sin(Qx2*tx/2)*2*sin(Qy2*ty/2)*(cos(Qz2*tz)-1+isin(Qz2*tz))/(iQx2*Qy2*Qz2));    %Form factor for corner molecule
        
        Rec(l,m) = (1/(Qx2*Qy2*Qz2))*dh*2*sin(Qx2*hx/2)*((cos(Qy2*hy/2)-cos(Qy2*ty/2))*(1-cos(Qz2*hz))+(sin(Qy2*hy/2)+sin(Qy2*ty/2))*sin(Qz2*hz))+...
            (1/(Qx2*Qy2*Qz2))*dt*4*sin(Qx2*tx/2)*sin(Qy2*ty/2)*sin(Qz2*tz);
        
        Imc(l,m) = (-1/(Qx2*Qy2*Qz2))*dh*2*sin(Qx2*hx/2)*((sin(Qy2*hy/2)+sin(Qy2*ty/2))*(1-cos(Qz2*hz))-sin(Qz2*hz)*(cos(Qy2*hy/2)-cos(Qy2*ty/2)))+...
            (-1/(Qx2*Qy2*Qz2))*dt*4*sin(Qx2*tx/2)*sin(Qy2*ty/2)*(cos(Qz2*tz)-1);
        
        %F(l,m) = (Re0(l,m) + 1i*Im0(l,m)) + (Rec(l,m) + 1i*Imc(l,m)) * (cos(Qz2*tail)-1i*sin(Qz2*tail)) * (-1)^(h(r)+k(s)); %Form factor for the unit cell
        
        Re(l,m) = Re0(l,m) + (Rec(l,m)*cos(Qz*tail)+Imc(l,m)*sin(Qz*tail))*(-1)^(h(r)+k(s));
        
        Im(l,m) = Im0(l,m) + (Imc(l,m)*cos(Qz*tail)-Rec(l,m)*sin(Qz*tail))*(-1)^(h(r)+k(s));
        
        F(l,m) = (Re(l,m))^2+(Im(l,m))^2;
        
        end
        
        Is(l) = Scale*sum(F(l,:).*S(l,:))*Dth*exp(-1 * q2(l)^2 * Deb^2);
     
        else
            
        Is(l) = 0;
        
        end
     
     end
     
     I_fin = I_fin + Is;
     
     end
   end
end

plot(q, I, 'r','Linewidth', 4);

xlim([0.5 3]);

hold on

plot(q2, I_fin, 'k','Linewidth', 2);
        
set(gca,'FontSize',16,'LineWidth',1, 'FontWeight', 'bold', 'FontName','Arial');

xlabel('q (A^-^1)', 'FontSize', 18, 'FontName','Arial', 'FontWeight', 'bold');

ylabel('I (cm^-^1)', 'fontsize', 18, 'FontName','Arial', 'FontWeight', 'bold');

title('WAXS fitting', 'fontsize', 16, 'FontName','Arial', 'FontWeight', 'bold');
        
        
        
        